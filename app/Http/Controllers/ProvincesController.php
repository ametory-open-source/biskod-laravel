<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ProvincesDataTable;

class ProvincesController extends Controller
{
    public function index(ProvincesDataTable $dataTable)
    {
        return $dataTable->render('provinces.index');
    }
}