<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Web;
use App\Models\Module;
use App\Models\Newsletter;
use App\Models\Page;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\BlogCategory;
use App\Models\Comment;
use App\Models\ContactUs;
use Vedmant\FeedReader\Facades\FeedReader;
use Illuminate\Support\Facades\Http;

class LandingController extends Controller
{
    public function main() 
    {
        $web = Web::first();
        if (!$web) $web = new Web();
        $rss = [];
        // if ($web->blog_rss_url) {
        //     $f = FeedReader::read($web->blog_rss_url);
        //     foreach ($f->get_items() as $key => $item) {
        //         $rss[$key]["title"] = $item->get_title();
        //         $rss[$key]["description"] = $item->get_content();
        //         $rss[$key]["short"] = limit_text(strip_tags($item->get_content()), 20);
        //         $rss[$key]["link"] = $item->get_link();
        //         preg_match('/<img(?: [^<>]*?)?src=([\'"])(.*?)\1/', $item->get_content(), $matches);
        //         $rss[$key]["img"] = ($matches) ?  end($matches) : null;
        //         # code...
        //     }
        // }

        // $rss = array_slice($rss, 0, 5);

        $banners = Module::where("type", "BANNER")->where('flag', true)->orderBy("sort")->get();
        $cs = Module::where("type", "CS")->where('flag', true)->orderBy("sort")->get();
        $cases = Module::where("type", "CASE")->where('flag', true)->orderBy("sort")->get();
        $team_heros = Module::where("type", "TEAM_HERO")->where('flag', true)->limit(4)->orderBy("sort")->get();
        $clients = Module::where("type", "CLIENT")->where('flag', true)->orderBy("sort")->get();
        $client_logos = Module::where("type", "CLIENT_LOGO")->where('flag', true)->orderBy("sort")->get();

        $blogs = Blog::where('status', "PUBLISHED")->limit(5)->orderBy("created_at", "desc")->get();

        $cs = $cs->chunk(3);
        $team_heros = $team_heros->chunk(2);
       
        
        return view("home", compact("web","banners", "cs", "cases", "team_heros", "clients", "client_logos", "blogs"));
    }

    public function save_template(Request $request) 
    {
        try {
            $input = $request->except(["_token"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();


            foreach (config("landing.pictures") as $key => $pic) {
                if ($request->has($pic)) {
                    $path = $request->file($pic)->store('public/images', 'local');
                    $input[$pic] = str_replace("public", "", $path);
                }
            }


            $input["copyright"] = $input["copyright"] ?? "";

            foreach (config("landing.show_modules") as $key => $item) {
                $input[$item] = isset($input[$item]) ? true : false;
            }
            $input["is_online"] = isset($input["is_online"]) ? true : false;
            
            $web = Web::first();
            if ($web) {
                $web->update($input);
            } else {
                Web::create($input);
            }
            return back()->with("success", "Template Updated");
        } catch (\Throwable $th) {
            dd($th);
        }
        
        
    }
    
    public function upload(Request $request) 
    {
        $path = $request->file('file')->store('public/images', 'local');
        $data = [
            "file" => str_replace("public", "", $path),
            "url" => assetUrl(str_replace("public", "", $path)),
        ];
        return response()->json($data) ;
    }

    public function template() 
    {
        $web = Web::first();
        if (!$web) $web = new Web();
        return view("template", compact("web"));
    }

    public function page(Request $request, $slug) 
    {
        $web = Web::first();
        $page = Page::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();
        
        return view('static.page', compact("page", "web"));
    }

    public function gallery(Request $request, Gallery $gallery) 
    {
        $web = Web::first();
        return view('static.gallery', compact("gallery", "web"));
    }

    public function galleryList(Request $request) 
    {

        $web = Web::first();
        $galleries = Gallery::where("status", "PUBLISHED")->orderBy("galleries.created_at", "desc")->paginate();

        return view("static.galleryList", compact("web", "galleries"));
    }

    public function contact(Request $request) 
    {

        $web = Web::first();

        return view("static.contact", compact("web"));
    }
    public function contactSend(Request $request) 
    {

        try {
            $input = $request->except(["_token"]);
            ContactUs::create($input);
            return back()->with("success", "Thank you for your participation");
        } catch (\Throwable $th) {

            // dd($th);
            return back()->with("error", "Something error")->withInput();
        }        
    }
    public function blogList(Request $request) 
    {
        $web = Web::first();
        $blogs = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->orderBy("blogs.created_at", "desc")->paginate();

        $categories = BlogCategory::all();

        $recents = Blog::where("status", "PUBLISHED")->orderBy('created_at', "desc")->limit(5)->get();

        $categories = BlogCategory::get();

        $getTags = Blog::where("status", "PUBLISHED")->get(["tags"]);
        $tags = [];
        foreach($getTags as $tag) {
            if ($tag->tags)
            $tags = array_merge($tags, explode(",", $tag->tags));
        }


        return view("static.blogList", compact("web", "blogs", "categories" ,'recents', "categories", "tags"));
    }
    public function blog(Request $request, $slug) 
    {
        $web = Web::first();
        $blog = Blog::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();

        $prev = Blog::where("id", "<", $blog->id)->where("status", "PUBLISHED")->first();
        $next = Blog::where("id", ">", $blog->id)->where("status", "PUBLISHED")->first();

        if ($blog->short_description) {
            $web->description = $blog->short_description;
        }
        if ($blog->feature_image) {
            $web->image = assetUrl($blog->feature_image);
        }
        if ($blog->keywords) {
            $web->keywords = $blog->keywords;
        }

        $web->name .= " | ". $blog->title;

        $relateds = Blog::where("blog_category_id", $blog->blog_category_id)->where("status", "PUBLISHED")->where('id', "<>", $blog->id)->get();
        $recents = Blog::where("status", "PUBLISHED")->orderBy('created_at', "desc")->where('id', "<>", $blog->id)->limit(5)->get();

        $categories = BlogCategory::get();

        $getTags = Blog::where("status", "PUBLISHED")->get(["tags"]);
        $tags = [];
        foreach($getTags as $tag) {
            if ($tag->tags)
            $tags = array_merge($tags, explode(",", $tag->tags));
        }

        return view('static.blog', compact("blog", "web", "prev", "next", "relateds", 'recents', "categories", "tags"));
    }

    public function postComment(Request $request, Blog $blog) 
    {
        try {
            $input = $request->except(["_token"]);
            $input["status"] = $blog->comment_moderation ? "DRAFT" : "PUBLISHED";
            $msg = $blog->comment_moderation ? "Your comment will appear after being moderated by the admin" : "Thank you for your participation
            ";
            $input["blog_id"] = $blog->id;
            Comment::create($input);
            return back()->with("success", $msg);
        } catch (\Throwable $th) {

            // dd($th);
            return back()->with("error", "Something error")->withInput();
        }        
        
    }

    public function newsletter(Request $request) 
    {
        try {
            Newsletter::create($request->all());
            return response()->json(['status' => 'success', 'message' => "Terima  Kasih Telah Berlangganan"]);
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            
            if (str_contains($message, "Duplicate entry")) {
                $message = "EMAIL SUDAH TERDAFTAR";
            }
            return response()->json(['status' => 'failed', 'message' => $message]);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed', 'message' => "Berlanganan Gagal"]);
        }
        
    }

    public function pricing() 
    {
        $web = Web::first();
        $pricings = Module::where("type", "PRICING")->where('flag', true)->get();

        return view("static.pricing", compact("web", "pricings"));
    }
    
    public function faq() 
    {
        $web = Web::first();
        $questions = Module::where("type", "QUESTION")->where('flag', true)->get();

        return view("static.faq", compact("web", "questions"));
    }

    public function post_auth_login(Request $request) 
    {

        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Login", [
                'username' => $request->email,
                'password' => $request->password,
                'device' => "web"
            ]);
    
            if ($response->status() == 200) {
                $resp = $response->json();
                $token = $resp["token"];
                $default_company_id = $resp["default_company_id"];

                $link = env('APP_FRONTEND_URL')."/login?company_id=".$default_company_id."&token=".$token;
                return back()->with("redirect", $link)->withInput();
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }        
    }

    public function auth_login() 
    {
        $web = Web::first();
        

        return view("static.login", compact("web"));
    }
    public function post_join(Request $request) 
    {
        $input = $request->except("_token");

        // dd($input);
        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Join", $input);
            if ($response->status() == 200) {
                return back()->with("success", "Your submission will be reviewed by our admin, please wait for the confirmation email")->withInput();
            } else {
                throw new Exception($response->body());
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }        
    }
    public function join() 
    {
        $web = Web::first();
        

        return view("static.join", compact("web"));
    }

    

}
