<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\NewslettersDataTable;

class NewslettersController extends Controller
{
    public function index(NewslettersDataTable $dataTable)
    {
        return $dataTable->render('newsletters.index');
    }
}