<?php

function youtubeID($url)  {
    preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $results);    
    return end($results);
}

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function assetUrl($text) {
   
    return asset("/storage". $text);
}
function parsePricing($text) {
    $exp = explode("\n", $text);
    $list = [];
    foreach ($exp as $key => $item) {
        if (str_contains($item, "[-]")) {
            array_push($list, sprintf("<li class=\"not-included\">%s</li>", str_replace("[-]", "",$item)));
        } else {
            array_push($list, sprintf("<li>%s</li>", $item));
        }
    }
    return implode("\n",$list);
}



function convertKebabCase(string $string)
{
    // Replace repeated spaces to underscore
    $string = preg_replace('/[\s.]+/', '_', $string);
    // Replace un-willing chars to hyphen.
    $string = preg_replace('/[^0-9a-zA-Z_\-]/', '-', $string);
    // Skewer the capital letters
    $string = strtolower(preg_replace('/[A-Z]+/', '-\0', $string));
    $string = trim($string, '-_');

    return preg_replace('/[_\-][_\-]+/', '-', $string);
}