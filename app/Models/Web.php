<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    use HasFactory;

    
    protected $fillable = [
        "name",
        "short_description",
        "description",
        "meta",
        "styles",
        "scripts",
        
        "blog_title",
        "blog_url",
        "newsletter_title",
        "keywords",
        "header_menu",
        "desktop_menu",
        "logo",
        "loading",
        
        "copyright",
        "company_name",
        "company_phone",
        "company_email",
        "company_web",
        "company_coordinate",
        "company_address",
        "company_map",
        "is_online",
        "offline_template",
        "show_header",
        "show_banner",
        "show_experience",
        "show_development",
        "show_cs",
        "show_case",
        "show_call",
        "show_team_hero",
        "show_client",
        "show_blog",
        "show_brand",
        "show_footer",
        "experience_title",
        "experience_subtitle",
        "experience_list",
        "experience_info",
        "development_title",
        "development_subtitle",
        "development_list",
        "development_picture",
        "development_video_url",
        "development_link",
        "cs_title",
        "case_title",
        "case_subtitle",
        "case_categories",
        "call_banner",
        "call_title",
        "call_subtitle",
        "call_link",
        "team_hero_title",
        "footer_company_link",
        "footer_services_link",
        "facebook_link",
        "instagram_link",
        "twitter_link",
        "youtube_link",
        "team_hero_picture",
        "team_hero_info",
        "client_title",
    ];
}
