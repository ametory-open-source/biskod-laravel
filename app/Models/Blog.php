<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        "title",
        "slug",
        "feature_image",
        "feature_image_caption",
        "keywords",
        "blog_category_id",
        "short_description",
        "description",
        "tags",
        "status",
        "comment_enabled",
        "comment_moderation",
        "author",
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, "blog_category_id", "id");
    }
    public function comments_published()
    {
        return $this->hasMany(Comment::class)->where("status", "PUBLISHED")->whereNull("parent_id");
    }
    public function all_comments_published()
    {
        return $this->hasMany(Comment::class)->where("status", "PUBLISHED");
    }
}
