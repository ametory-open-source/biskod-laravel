<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "description",
        "picture",
    ];

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
    
    public function published_blogs()
    {
        return $this->hasMany(Blog::class)->where("status", "PUBLISHED");
    }
}
