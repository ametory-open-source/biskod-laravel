<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $fillable = [
        "icon",
        "picture",
        "type",
        "title",
        "subtitle",
        "price",
        "description",
        "link",
        "sort",
        "category",
    ];
}
