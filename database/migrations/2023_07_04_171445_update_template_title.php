<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach ([
            "step_title",
            "step_started_link",
            "step_learn_link",
            "growth_title",
            "growth_started_link",
            "growth_learn_link",
            "blog_rss_url",
            "hero_title",
            "hero_description",
            "footer_explore_link",
            "footer_help_link",
            "client_title",
            "client_subtitle",
            "client_description",
        ] as $key => $value) {
            $this->myDropColumnIfExists('webs', $value);
        }
        
        Schema::table('webs', function (Blueprint $table) {
            $table->text("experience_title");
            $table->text("experience_subtitle");
            $table->text("experience_list");
            $table->text("experience_info");
            $table->text("development_title");
            $table->text("development_subtitle");
            $table->text("development_list");
            $table->string("development_picture");
            $table->string("development_video_url");
            $table->string("cs_title");
            $table->string("case_title");
            $table->string("case_subtitle");
            $table->string("case_categories");
            $table->string("call_banner");
            $table->string("call_title");
            $table->string("call_subtitle");
            $table->string("call_link");
            $table->string("team_hero_title");
            $table->string("footer_company_link");
            $table->string("footer_services_link");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "experience_title",
                "experience_subtitle",
                "experience_list",
                "experience_info",
                "development_title",
                "development_subtitle",
                "development_list",
                "development_picture",
                "development_video_url",
                "cs_title",
                "case_title",
                "case_subtitle",
                "case_categories",
                "call_banner",
                "call_title",
                "call_subtitle",
                "call_link",
                "team_hero_title",
                "footer_company_link",
                "footer_services_link",
            ]);
        });
    }



    function myDropColumnIfExists($myTable, $column)
    {
        if (Schema::hasColumn($myTable, $column)) //check the column
        {
            Schema::table($myTable, function (Blueprint $table) use($column)
            {
                $table->dropColumn($column); //drop it
            });
        }

    }
};


