<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("facebook_link");
            $table->string("instagram_link");
            $table->string("twitter_link");
            $table->string("youtube_link");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn("facebook_link");
            $table->dropColumn("instagram_link");
            $table->dropColumn("twitter_link");
            $table->dropColumn("youtube_link");
        });
    }
};
