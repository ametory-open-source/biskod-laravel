<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('webs', function (Blueprint $table) {
            $table->id();
            $table->string("name")->default("");
            $table->string("short_description")->default("");
            $table->string("description")->default("");
            $table->longtext("meta");
            $table->longtext("styles");
            $table->longtext("scripts");
            $table->boolean("show_header");
            $table->boolean("show_hero");
            $table->boolean("show_video");
            $table->boolean("show_growth");
            $table->boolean("show_step");
            $table->boolean("show_client");
            $table->boolean("show_question");
            $table->boolean("show_pricing");
            $table->boolean("show_screenshot");
            $table->boolean("show_blog");
            $table->boolean("show_newsletter");
            $table->boolean("show_footer");
            $table->string("playstore_link")->default("");
            $table->string("appstore_link")->default("");
            $table->string("hero_title")->default("");
            $table->string("hero_description")->default("");
            $table->string("feature_title")->default("");
            $table->string("video_link")->default("");
            $table->string("growth_title")->default("");
            $table->string("growth_started_link")->default("");
            $table->string("growth_learn_link")->default("");
            $table->string("step_title")->default("");
            $table->string("step_started_link")->default("");
            $table->string("step_learn_link")->default("");
            $table->string("client_title")->default("");
            $table->string("client_subtitle")->default("");
            $table->string("client_description")->default("");
            $table->string("question_title")->default("");
            $table->string("pricing_title")->default("");
            $table->string("screenshot_title")->default("");
            $table->string("blog_title")->default("");
            $table->string("blog_rss_url")->default("");
            $table->string("newsletter_title")->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('webs');
    }
};
