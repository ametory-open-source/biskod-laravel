<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach ([
            "show_header",
            "show_hero",
            "show_video",
            "show_growth",
            "show_feature",
            "show_step",
            "show_client",
            "show_question",
            "show_pricing",
            "show_screenshot",
            "show_blog",
            "show_newsletter",
            "show_footer",
            "feature_title",
            "playstore_link",
            "appstore_link",
            "video_link",
            "question_title",
            "pricing_title",
            "screenshot_title",
            "hero_picture",
            "video_picture",
            "question_picture",
            "subscribe_picture",
        ] as $key => $value) {
            $this->myDropColumnIfExists('webs', $value);
        }

        
        Schema::table('webs', function (Blueprint $table) {
            $table->boolean("show_header")->default(true);
            $table->boolean("show_banner")->default(true);
            $table->boolean("show_experience")->default(true);
            $table->boolean("show_development")->default(true);
            $table->boolean("show_cs")->default(true);
            $table->boolean("show_case")->default(true);
            $table->boolean("show_call")->default(true);
            $table->boolean("show_team_hero")->default(true);
            $table->boolean("show_client")->default(true);
            $table->boolean("show_blog")->default(true);
            $table->boolean("show_brand")->default(true);
            $table->boolean("show_footer")->default(true);
           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "show_header",
                "show_banner",
                "show_experience",
                "show_development",
                "show_cs",
                "show_case",
                "show_call",
                "show_team_hero",
                "show_client",
                "show_blog",
                "show_brand",
                "show_footer",
            ]);
        });
    }

    function myDropColumnIfExists($myTable, $column)
    {
        if (Schema::hasColumn($myTable, $column)) //check the column
        {
            Schema::table($myTable, function (Blueprint $table) use($column)
            {
                $table->dropColumn($column); //drop it
            });
        }

    }
};
