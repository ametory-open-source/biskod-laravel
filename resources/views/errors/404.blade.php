@extends("layouts.landing")

@section('content')
    <div class="container d-flex">
        <img src="/assets/images/404.jpg" alt="" srcset="" class="img-responsive img">
    </div>
@endsection

@push("css")
    <style>
        .img {
            width: 60%;
            margin: auto;
            margin-top: 200px;
        }
    </style>
@endpush