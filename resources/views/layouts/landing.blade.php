@if($web->is_online)
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{!! $web->name !!}</title>
    <meta name="description" content="{!! $web->description !!}">
    <meta name="title" content="{!! $web->name !!}">
    <meta name="keywords" content="{!! $web->keywords !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{!! $web->name !!}" />
    <meta property="og:description" content="{!! $web->description !!}" />
    <meta property="og:site_name" content="{!! $web->short_description !!}">
    <meta property="og:url" content="{{ url("/") }}">
    @if($web->image)
    <meta property="og:image" content="{!! $web->image !!}" />
    @endif
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!-- fontawesome css link -->
    <link rel="stylesheet" href="/web/assets/css/fontawesome-all.min.css">
    <!-- bootstrap css link -->
    <link rel="stylesheet" href="/web/assets/css/bootstrap.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="/web/assets/images/fav.png" type="image/x-icon">
    <!-- swipper css link -->
    <link rel="stylesheet" href="/web/assets/css/swiper.min.css">
    <!-- lightcase css links -->
    <link rel="stylesheet" href="/web/assets/css/lightcase.css">
    <!-- odometer css link -->
    <link rel="stylesheet" href="/web/assets/css/odometer.css">
    <!-- line-awesome-icon css -->
    <link rel="stylesheet" href="/web/assets/css/icomoon.css">
    <!-- line-awesome-icon css -->
    <link rel="stylesheet" href="/web/assets/css/line-awesome.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="/web/assets/css/animate.css">
    <!-- aos.css -->
    <link rel="stylesheet" href="/web/assets/css/aos.css">
    <!-- nice select css -->
    <link rel="stylesheet" href="/web/assets/css/nice-select.css">
    <!-- main style css link -->
    <link rel="stylesheet" href="/web/assets/css/style.css">
    <!-- Responsive Css -->
    <link rel="stylesheet" href="/web/assets/css/responsive.css">
    @stack("css")
</head>

<body>

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Preloader
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div class="preloader">
        <div class="drawing" id="loading">
            <div class="loading-dot"></div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Preloader
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


    <div class="cursor"></div>
    <div class="cursor-follower"></div>
    
    @include("components.landing.header")

    @yield("content")
    @include("components.landing.footer")

    <!-- jquery -->
    <script src="/web/assets/js/jquery-3.6.0.min.js"></script>
    <!-- bootstrap js -->
    <script src="/web/assets/js/bootstrap.min.js"></script>
    <!-- swipper js -->
    <script src="/web/assets/js/swiper.min.js"></script>
    <!-- lightcase js-->
    <script src="/web/assets/js/lightcase.js"></script>
    <!-- odometer js -->
    <script src="/web/assets/js/odometer.min.js"></script>
    <!-- viewport js -->
    <script src="/web/assets/js/viewport.jquery.js"></script>
    <!-- aos js file -->
    <script src="/web/assets/js/aos.js"></script>
    <!-- nice select js -->
    <script src="/web/assets/js/jquery.nice-select.js"></script>
    <!-- isotope js -->
    <script src="/web/assets/js/isotope.pkgd.min.js"></script>
    <!-- tweenMax js -->
    <script src="/web/assets/js/TweenMax.min.js"></script>
    <!-- main -->
    <script src="/web/assets/js/main.js"></script>
    @stack("scripts")

</body>

</html>

@else
{!! $web->offline_template !!}
@endif