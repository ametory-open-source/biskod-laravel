
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Footer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<footer class="footer-section three pt-120">
  <div class="container">
      <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="footer-contact">
                  <div class="content">
                      <h4 class="title">Start Your Projects Today</h4>
                  </div>
                  <div class="contact">
                      <i class="fas fa-phone-alt"></i>
                      <p><a href="tel:{{ $web->company_phone }}">{{ $web->company_phone }}</a></p>
                  </div>
              </div>
          </div>
      </div>
      <div class="row mb-30-none">
          <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 mb-30">
              <div class="footer-widget">
                  <h5 class="title style-01">About us</h5>
                  <p>{{ $web->short_description }}</p>
                  <ul class="footer-social style-01">
                      <li><a target="_blank" href="{{ $web->facebook_link }}"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a target="_blank" href="{{ $web->instagram_link }}"><i class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 mb-30">
              <div class="footer-widget">
                  <h5 class="title style-01">Company</h5>
                  <ul class="footer-list style-01">
                    {!! $web->footer_company_link !!}
                  </ul>
              </div>
          </div>
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 mb-30">
              <div class="footer-widget">
                  <h4 class="title style-01">Services</h4>
                  <ul class="footer-list style-01">
                    {!! $web->footer_services_link !!}
                  </ul>
              </div>
          </div>
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 mb-30">
              <div class="footer-widget">
                  <h4 class="title style-01">Get in Touch</h4>
                  <div class="footer-subscribe">
                    <form id="form" action="/newsletter" method="post" >
                        @csrf
                      <div class="form-group">
                           <input type="email" name="email"  id="email" class="form-control" required placeholder="Email">
                      </div>
                      <div class="subscribe-btn">
                          <button type="submit" class="btn--base subscribe">Subscribe Now</button>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="copyright-wrapper three">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-xl-12 text-center">
                  <div class="copyright-area style-01">
                      <div class="logo">
                          <img src="{{ assetUrl($web->logo) }}" width="100" alt="">
                      </div>
                      <p> {{ $web->copyright }}</p>
                  </div>
              </div>
          </div>
      </div>
  </div>
</footer>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  End Footer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


@push("scripts")
<script>
    $(function() {
            $("#form").submit(function (event) {
                var formData = {
                    email: $("#email").val(),
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                };
                $.ajax({
                    type: "POST",
                    url: "/newsletter",
                    data: formData,
                    dataType: "json",
                    encode: true,
                }).done(function (data) {
                    alert(data.message);
                    $("#email").val("")
                });

                event.preventDefault();
            });
        })
</script>
@endpush