<!--questions section start-->
<section class="questions" id="faq">
    <div class="questions__wrapper">
        <div class="container">
            <h2 class="section-heading color-black">{!! $web->question_title !!}</h2>
            <div class="row align-items-lg-center">
                <div class="col-lg-12 order-2 order-lg-1">
                    <div id="accordion">
                        @foreach ($questions as $i => $item)
                        <div class="card" id="card-{{ $i+1 }}">
                            <div class="card-header" id="heading-{{ $i+1 }}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $i+1 }}"
                                        aria-expanded="true" aria-controls="collapse-{{ $i+1 }}">
                                        {{ $item->title }}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-{{ $i+1 }}" class="collapse {{ $i == 0 ? 'show' : null }}" aria-labelledby="heading-{{ $i+1 }}"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="paragraph">{{ $item->description }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
              
            </div>
        </div>
    </div>
</section>
<!--questions section end-->
@push("css")
    <style>
        .questions__wrapper {
            background-color: transparent;
        }
        .questions .section-heading {
            width: 100%;
            margin-top: 100px;
            margin-bottom: 50px;
        }
        .questions .card .card-body {
            background-color: #f2f2f2;
        }
        .questions .card .card-body::before {
            background: none;
        }
    </style>
@endpush