<!--questions section start-->
<section class="questions" id="faq">
    <div class="questions__wrapper">
        <div class="container">
            <h2 class="section-heading color-black">{!! $web->question_title !!}</h2>
            <div class="row align-items-lg-center">
                <div class="col-lg-6 order-2 order-lg-1">
                    <div id="accordion">
                        @foreach ($questions as $i => $item)
                        <div class="card" id="card-{{ $i+1 }}">
                            <div class="card-header" id="heading-{{ $i+1 }}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $i+1 }}"
                                        aria-expanded="true" aria-controls="collapse-{{ $i+1 }}">
                                        {{ $item->title }}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-{{ $i+1 }}" class="collapse {{ $i == 0 ? 'show' : null }}" aria-labelledby="heading-{{ $i+1 }}"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="paragraph">{{ $item->description }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <div class="questions-img">
                        @if($web->question_picture)
                        <img src="{{ asset('/storage'. $web->question_picture) }}" alt="image">
                        @else
                        <img src="/web/assets/images/phone-01.png" alt="image">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--questions section end-->