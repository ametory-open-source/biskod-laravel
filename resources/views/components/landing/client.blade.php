
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Happy Client Sction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="happy-client-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 text-center">
                <div class="section-header">
                    <h2 class="section-title">{{ $web->client_title }}</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center align-items-end mb-30-none">
            <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
                <div class="client-slider-area two">
                    <div class="client-slider-two">
                        <div class="swiper-wrapper">
                            @foreach ($clients as $item)
                            @php
                                $client = explode("|", $item->subtitle)
                            @endphp
                            <div class="swiper-slide">
                                <div class="client-single-item">
                                    <div class="content">
                                        <div class="icon">
                                            <i class="fas {{$item->icon ?? 'fa-quote-right'}}"></i>
                                            <h4 class="title">{{ $item->title }}</h4>
                                        </div>
                                        <p>{{ $item->description }}</p>
                                        <div class="designation-wrap">
                                            <div class="designation">
                                                <h6 class="name">{{ $client[0] }}</h6>
                                                @isset($client[1])
                                                    <span class="position">{{ $client[1] }}</span>    
                                                @endisset
                                            
                                                
                                            </div>
                                            <div class="ratings">
                                                @for ($i=0; $i < $item->price ?? 5 ; $i++)
                                                    <i class="fas fa-star"></i>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Happy Client Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
@endpush