
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Blog Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="blog-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 text-center">
                <div class="section-header">
                    <h2 class="section-title">{{ $web->blog_title }}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($blogs->slice(0,2) as $item)
            <div class="col-lg-4 mb-30">
                <div class="blog-single-item">
                    <div class="thumbnail" style="background: url({{ assetUrl($item->feature_image) }}) no-repeat; height: 400px;background-size: cover;">
                    </div>
                    <div class="content">
                        <a href="/blog?category={{ optional($item->category)->name }}"><span class="event">{{ optional($item->category)->name }}</span></a>
                        <a href="{{ url('blog/'.$item->slug) }}"><h4 class="title">{{ $item->title }}</h4></a>
                        <div class="post-meta">
                            <span class="user">By : {{ $item->author }}</span>
                            <span class="date">{{ $item->created_at->format("d/M/Y") }}</span>
                        </div>
                    </div>
                </div>
            </div>    
            @endforeach
            <div class="col-lg-4 mb-30">
                <div class="blog-single-item-02">
                    @foreach ($blogs->slice(2, 3) as $item)
                        <div class="content">
                            <a href="{{ url('blog/'.$item->slug) }}"><h4 class="title">{{ $item->title }}</h4></a>
                            <div class="post-meta">
                                <span class="user">By : {{ $item->author }}</span>
                                <span class="date">{{ $item->created_at->format("d/M/Y") }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Blog Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->