<!--video section start-->
<div class="video" id="video">
    <div class="video__wrapper">
        <div class="container">
            <div class="video__play">
                <button type="button" data-toggle="modal" data-target="#videoModal">
                    <i class="fad fa-play"></i>
                </button>
                <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-close">
                                <button type="button" data-dismiss="modal" aria-label="Close">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="ytdefer yt-video" style="width: 100%; height: 100%;" data-src="{{ $web->video_link ? youtubeID($web->video_link) : '2BrCE_zxM0U'}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="video__background">
                <img src="/web/assets/images/video-bg-1.png" alt="image" class="texture-1">
                @if($web->video_picture)
                <img src="{{ asset('/storage'. $web->video_picture) }}" alt="image" class="phone">
                @else
                <img src="/web/assets/images/video-img.png" alt="image" class="phone">
                @endif
                <img src="/web/assets/images/video-bg-2.png" alt="image" class="texture-2">
            </div>
        </div>
    </div>
</div>
<!--video section end-->