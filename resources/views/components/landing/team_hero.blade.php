
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Team Hero Sction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="team-hero-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <div class="hero-single-item">
                    <div class="content">
                        <h6 class="title-main">{{ $web->team_hero_title }}</h6>
                        <div class="thumbnail">
                            <img src="{{ assetUrl($web->team_hero_picture) }}" alt="">
                        </div>
                        <div class="counter-single-items">
                            {!! $web->team_hero_info !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="row align-items-center">
                    @foreach ($team_heros as $group)
                    <div class="col-lg-6 col-md-6">
                        @foreach ($group as $item)
                        <div class="team-thumbnail mb-30">
                            <img src="{{ assetUrl($item->picture) }}" alt="{{ $item->title }}">
                        </div>    
                        @endforeach
                    </div>    
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Team Hero Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
