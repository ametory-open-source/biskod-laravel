<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="banner-section home-three">
    <div class="banner-three-slider">
        <div class="swiper-wrapper">
            @foreach ($banners as $item)
            <div class="swiper-slide">
                <div class="container custom-container-three">
                    <div class="home-three-banner">
                        <img src="/web/assets/images/home-three/Ellipse.png" class="banner-shape" alt="">
                        <div class="row align-items-end mb-30-none">
                            <div class="col-xl-5 col-lg-6">
                                <div class="banner-content-two" data-aos="fade-right" data-aos-duration="1800">
                                    <h1 class="title">{{ $item->title }}</h1>
                                    <span></span>
                                    {!! $item->description !!}
                                </div>
                            </div>
                            <div class="banner-image">
                                <img src="{{ assetUrl($item->picture) }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
        <div class="custom-navigation next-text">NEXT</div>
        <div class="custom-navigation prev-text">PREV</div>
        <div class="custom-pagination"></div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->