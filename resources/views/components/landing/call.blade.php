
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Call to action
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="call-to-action-section">
    <div class="call-to-action-element" data-aos="fade-left" data-aos-duration="1200">
        <img src="/web/assets/images/element/element-33.png" alt="element">
    </div>
    <div class="wrapper demo-text">
        <div class="marquee">
            <span>
                {{ $web->call_banner }}
            </span>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9">
                <div class="call-to-action-wrapper">
                    <div class="call-to-action-content">
                        <h3 class="title">{{ $web->call_title }}</h3>
                        <h3 class="inner-title">{{ $web->call_subtitle }}</h3>
                    </div>
                    <div class="call-to-action-btn">
                        <a href="{{ $web->call_link }}" class="btn--base">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Call to action
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

