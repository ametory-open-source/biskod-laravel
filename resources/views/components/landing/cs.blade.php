
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Customer Service Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="customer-service-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 text-center">
                <div class="section-header">
                    <h2 class="section-title">{{ $web->cs_title }}</h2>
                </div>
            </div>
        </div>
        @foreach ($cs as $key=> $group)
        <div class="row {{$key ? 'rmt-60' : null }}">
            @foreach ($group as $item)
            <div class="col-xl-4 col-lg-4 mb-30">
                <div class="service-single-item">
                    <div class="icon">
                        <img src="{{assetUrl($item["picture"])}}" alt="">
                    </div>
                    <div class="content">
                        <a href="{{ $item["link"] }}"><h6 class="title">{{ $item["title"] }}</h6></a>
                        <p>{{ $item["description"] }}</p>
                        <div class="more-btn">
                            <a href="{{ $item["link"] }}"><span>Service Details</span></a>
                            <div class="icons">
                                <i class="las la-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            @endforeach
        </div>
        @endforeach
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Customer Service Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
