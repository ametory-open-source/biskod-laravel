<!--feature section start-->
<section class="feature" id="intro">
    <div class="container">
        <h2 class="section-heading color-black">{!! $web->feature_title !!}</h2>
        <div class="row">
            @foreach ($features as $i => $item)
                <div class="col-lg-3 col-md-6">
                    <div class="feature__box feature__box--{{ $i + 1}}">
                        <div class="icon icon-{{ $i + 1}}">
                            <i class="fad {{ $item->icon }}"></i>
                        </div>
                        <div class="feature__box__wrapper">
                            <div class="feature__box--content feature__box--content-{{ $i + 1}}">
                                <h3>{{ $item->title }}</h3>
                                <p class="paragraph dark">{{ $item->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--feature section end-->