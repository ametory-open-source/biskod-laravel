
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Header
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<header class="header-section home-three">
  <div class="header">
      <div class="header-bottom-area home-three">
          <div class="container custom-container-three">
              <div class="header-menu-content">
                  <nav class="navbar home-three navbar-expand-xl p-0">
                      <a class="site-logo site-title home-three-logo" href="/"><img src="{{ assetUrl($web->logo) }}" alt="site-logo" width="100"></a>
                      <button class="navbar-toggler home-three d-block d-xl-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="toggle-bar"></span>
                      </button>
                      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                          <div class="header-right">
                              <button class="menu-toggler style-01 home-three ml-auto">
                                  <span class="toggle-bar home-three"></span>
                              </button>

                              <div class="menu-toggler-wrapper home-three d-block d-xl-none">
                                  <ul class="navbar-nav main-menu style-01">
                                    {!! $web->header_menu !!}
                                  </ul>
                              </div>
                          </div>
                      </div>
                      <div class="logo-wrapper">
                          <a class="site-title" href="/"><img src="{{ assetUrl($web->logo) }}" alt="site-logo" width="100"></a>
                      </div>
                      <div class="touch">
                          <a href="/contact"><span>Get In touch</span></a>
                          <div class="icon">
                              <i class="las la-arrow-right"></i>
                          </div>
                      </div>
                  </nav>
              </div>
          </div>
      </div>
  </div>
</header>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  End Header
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Desktop Creative Menu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="home-three-menu">
    <div class="menu-open">
        <div class="close-btn">
            <div class="logo">
                <a class="site-logo site-title home-three-logo" href="/"><img src="{{ assetUrl($web->logo) }}" alt="site-logo" width="100"></a>
            </div>
            <div class="cross-btn">
                <a href="javascript:void(0)">
                    <i class="las la-times"></i>
                </a>
            </div>
        </div>
        <div class="nav-wrapper">
            <span class="menu-text">Menu</span>
            <ul class="navigation-three">
                {!! $web->desktop_menu !!}
            </ul>
            <div class="address-wrapper">
                <div class="address">
                    <h6 class="title">Address</h6>
                    <p>{{ $web->company_address}}</p>
                </div>
                <div class="contact">
                    <h6 class="title">Contact</h6>
                    <p><a href="tel:{{ $web->company_phone}}">{{ $web->company_phone}}</a></p>
                    <p><a href="mailto:{{ $web->company_email}}">{{ $web->company_email}}</a></p>
                </div>
                <ul class="footer-social">
                    <h6 class="title">Follow Us</h6>
                    <li><a href="{{ $web->facebook_link}}"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="{{ $web->instagram_link}}"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>  
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Desktop Creative Menu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

