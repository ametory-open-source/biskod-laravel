
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Experience Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="experience-secction">
    <div class="container custom-container-four">
        <div class="row">
            <div class="col-xl-6 col-lg-6 mb-30">
                <div class="experience-content">
                    <h4 class="title">{{ $web->experience_title}}</h4>
                    <ul class="plan-list">
                        @foreach (explode("\n", $web->experience_list) as $item)
                        <li>{{ $item }}</li>    
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 mb-30">
                <div class="statistics-item-area">
                    <p>{{ $web->experience_subtitle}}</p>
                    {!! $web->experience_info !!}
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Experience Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
