<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Case Study Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="case-study-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-header-wrapper">
                    <div class="section-header">
                        <h2 class="section-title">{{ $web->case_title }}</h2>
                        <p>{{ $web->case_subtitle }}</p>
                    </div>
                    <ul class="nav nav-pills mb-3">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#all">All</a>
                        </li>
                        @foreach (explode(",", $web->case_categories) as $item)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#{{ convertKebabCase($item) }}">{{ $item }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="all">
            <div class="container-fluid">
                <div class="row mb-10-none">
                    <div class="col-xl-10 offset-xl-2">
                        <div class="case-study-slider-wrapper">
                            <div class="case-study-slider oh">
                                <div class="swiper-wrapper">
                                    @foreach ($cases as $case)
                                    <div class="swiper-slide">
                                        <div class="case-study-single-item">
                                            <div class="thumbnail">
                                                <img src="{{ assetUrl($case->picture) }}" alt="">
                                            </div>
                                            <div class="content">
                                                <h5 class="title">{{ $case->title }}</h5>
                                                <p>{{ $case->description }}</p>
                                            </div>
                                        </div>
                                    </div>    
                                    @endforeach
                                    
                                    
                                </div>
                                <div class="next-text">Next</div>
                                <div class="prev-text">Prev</div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach (explode(",", $web->case_categories) as $item)
        <div class="tab-pane fade" id="{{ convertKebabCase($item) }}">
            <div class="container-fluid">
                <div class="row mb-10-none">
                    <div class="col-xl-10 offset-xl-2">
                        <div class="case-study-slider-wrapper">
                            <div class="case-study-slider oh">
                                <div class="swiper-wrapper">
                                    @foreach ($cases->where("category", $item) as $case)
                                    <div class="swiper-slide">
                                        <div class="case-study-single-item">
                                            <div class="thumbnail">
                                                <img src="{{ assetUrl($case->picture) }}" alt="">
                                            </div>
                                            <div class="content">
                                                <h5 class="title">{{ $case->title }}</h5>
                                                <p>{{ $case->description }}</p>
                                            </div>
                                        </div>
                                    </div>   
                                    @endforeach
                                </div>
                                <div class="next-text">Next</div>
                                <div class="prev-text">Prev</div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Case Study Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->