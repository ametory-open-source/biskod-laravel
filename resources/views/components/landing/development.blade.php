
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Development Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="development-section" style="background-image: url(web/assets/images/home-three/dev-bg.png);">
    <div class="container">
         <img src="/web/assets/images/home-three/grid.png" class="shape-01" alt="">
         <img src="/web/assets/images/home-three/grid.png" class="shape-02" alt="">
        <div class="row">
            <div class="col-xl-6 col-lg-6 mb-30">
                <div class="thumbnail">
                    <img src="{{ assetUrl($web->development_picture) }}" alt="">
                    <div class="about-thumb-video">
                        <div class="circle">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                <defs>
                                    <path id="circlePath" d=" M 150, 150 m -60, 0 a 60,60 0 0,1 120,0 a 60,60 0 0,1 -120,0 "></path>
                                </defs>
                                <circle cx="150" cy="100" r="75" fill="none"></circle>
                                <g>
                                    <use xlink:href="#circlePath" fill="none"></use>
                                    <text fill="#606F92">
                                        <textPath xlink:href="#circlePath">{{ $web->development_subtitle }}</textPath>
                                    </text>
                                </g>
                            </svg>
                        </div>
                        <div class="video-main">
                            <a class="video-icon video" data-rel="lightcase:myCollection" href="{{ $web->development_video_url }}">
                                <i class="fas fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 mb-30">
                <div class="content">
                    <div class="title">{{ $web->development_title }}</div>
                    <p>{{ $web->development_subtitle }}</p>
                    <ul class="plan-list">
                        @foreach (explode("\n", $web->development_list) as $item)
                        <li>{{ $item }}</li>    
                        @endforeach
                    </ul>
                    <div class="more-btn">
                        <a href="{{ $web->development_link }}"><span>Know More</span></a>
                        <div class="icon">
                             <i class="las la-arrow-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Development Section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
