@extends("layouts.landing")

@section('content')

@includeWhen($web->show_banner, "components.landing.banner")
@includeWhen($web->show_experience, "components.landing.experience")
@includeWhen($web->show_development, "components.landing.development")
@includeWhen($web->show_cs, "components.landing.cs")
@includeWhen($web->show_case, "components.landing.case")





<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<a href="#" class="scrollToTop"><i class="las la-angle-double-up"></i></a>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

@includeWhen($web->show_call, "components.landing.call")
@includeWhen($web->show_team_hero, "components.landing.team_hero")
@includeWhen($web->show_client, "components.landing.client")
@includeWhen($web->show_blog, "components.landing.blog")
@includeWhen($web->show_brand, "components.landing.brand")

@endsection