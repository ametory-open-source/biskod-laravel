@extends("layouts.contact")

@section('content')

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="banner-section two inner">
    <div class="banner-element-four two">
        <img src="/web/assets/images/element/element-5.png" alt="element">
    </div>
    <div class="banner-element-five two">
        <img src="/web/assets/images/element/element-7.png" alt="element">
    </div>
    <div class="banner-element-nineteen two">
        <img src="/web/assets/images/element/element-6.png" alt="element">
    </div>
    <div class="banner-element-twenty-two two">
        <img src="/web/assets/images/element/element-69.png" alt="element">
    </div>
    <div class="banner-element-twenty-three two">
        <img src="/web/assets/images/element/element-70.png" alt="element">
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center mb-30-none">
            <div class="col-xl-12 mb-30">
                <div class="banner-content two">
                    <div class="banner-content-header">
                        <h2 class="title">{{ $blog->title }}</h2>
                        <div class="breadcrumb-area">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="/blog">Blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $blog->title }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<a href="#" class="scrollToTop"><i class="las la-angle-double-up"></i></a>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Blog
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="blog-section ptb-120">
    <div class="container">
        <div class="row justify-content-center mb-60-none">
            <div class="col-xl-8 col-lg-8 mb-60">
                <div class="row justify-content-center mb-60-none">
                    <div class="col-xl-12 mb-60">
                        <div class="blog-item details">
                            <div class="blog-thumb">
                                <img src="{{ assetUrl($blog->feature_image)}}" alt="{{ $blog->feature_image_caption }}">
                            </div>
                            <div class="blog-content">
                                <div class="blog-post-meta">
                                    <span class="user">By : {{ $blog->author }}</span>
                                    <span class="category two">{{ $blog->created_at->format("d/M/Y") }}</span>
                                </div>
                                <h3 class="title">{{ $blog->title }}</h3>
                                {!! $blog->description !!}
                                {!! $blog->tags !!}
                                <div class="blog-tag-wrapper">
                                    <span>Tags:</span>
                                    <ul class="blog-footer-tag">
                                        @foreach (explode(",", $blog->tags) as $item)
                                        <li><a href="/blog?tag={{ $item }}">{{ $item }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <nav>
                                    <ul class="pagination two">
                                        @if ($prev)
                                        <li class="page-item prev">
                                            <a class="page-link" href="/blog/{{$prev->slug}}" rel="prev"
                                                aria-label="Prev &raquo;"><i class="fas fa-chevron-left"></i></a>
                                        </li>
                                        @endif

                                        <li class="page-item tags"><a class="page-link" href="/blog"><i
                                                    class="icon-Tags_menu"></i></a></li>
                                        @if ($next)
                                        <li class="page-item next">
                                            <a class="page-link" href="/blog/{{$next->slug}}" rel="next"
                                                aria-label="Next &raquo;"><i class="fas fa-chevron-right"></i></a>
                                        </li>
                                        @endif
                                    </ul>
                                </nav>
                                @if ($relateds->count())

                                @endif
                                <div class="blog-related-area">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="blog-section-header">
                                                <div class="section-header">
                                                    <h3 class="section-title">Top Related Post</h3>
                                                </div>
                                                <div class="slider-nav-area">
                                                    <div class="slider-prev">
                                                        <i class="fas fa-chevron-left"></i>
                                                    </div>
                                                    <div class="slider-next">
                                                        <i class="fas fa-chevron-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-xl-12">
                                            <div class="blog-slider-area">
                                                <div class="blog-slider">
                                                    <div class="swiper-wrapper">
                                                        @foreach ($relateds as $item)
                                                        <div class="swiper-slide">
                                                            <div class="blog-item">
                                                                <div class="blog-thumb"
                                                                    style="background: url({{assetUrl($item->feature_image)}}) no-repeat; height: 300px;background-size: cover;">

                                                                </div>
                                                                <div class="blog-content">
                                                                    <div class="blog-post-meta">
                                                                        <span class="user">By : {{ $item->author
                                                                            }}</span>
                                                                        <span class="category two"> {{
                                                                            $item->created_at->format("d/M/Y") }}</span>
                                                                    </div>
                                                                    <h3 class="title"><a
                                                                            href="/blog/{{ $item->slug }}">{{
                                                                            $item->title }}</a></h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ($message = Session::get('success'))
                                <div class="blog-comment-area comment-area-message">
                                    <div style="padding:20px; cursor: pointer;" class="alert-wrapper"
                                        data-bs-dismiss="alert" aria-label="Close">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong style="font-size: 18pt;">{{ $message ?? "HALO" }}</strong>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="blog-comment-area">
                                    <h3 class="title">Leave A Comments</h3>
                                    <p>Your email address will not be published. Required fields are marked *</p>
                                    <form class="comment-form" action="/comments/{{$blog->id}}" method="post">
                                        @csrf
                                        <div class="row justify-content-center mb-25-none">
                                            <div class="col-xl-6 col-lg-6 form-group">
                                                <input value="{{ old('name') }}" type="text" name="name"
                                                    class="form--control" placeholder="Your name*">
                                            </div>
                                            <div class="col-xl-6 col-lg-6 form-group">
                                                <input value="{{ old('email') }}" type="email" name="email"
                                                    class="form--control" placeholder="Your email*">
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <textarea class="form--control" name="comment"
                                                    placeholder="Write message*">{{ old('comment') }}</textarea>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <button type="submit" class="btn--base mt-10">Submit Now <i
                                                        class="fas fa-arrow-right ml-2"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 mb-60">
                <div class="sidebar">
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Search</h4>
                        <div class="search-widget-box">
                            <form class="search-form" action="/blog">
                                <input type="text" name="search" class="form--control" placeholder="Search" >
                                <button type="submit"><i class="icon-Search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Recent Posts</h4>
                        <div class="popular-widget-box">
                            @foreach ($recents as $item)
                            <div class="single-popular-item d-flex flex-wrap align-items-center">
                                <div class="popular-item-thumb">
                                    <img src="{{ assetUrl($item->feature_image) }}"
                                        alt="{{ $item->feature_image_caption }}">
                                </div>
                                <div class="popular-item-content">
                                    <span class="blog-date">{{ $item->created_at->format("d/M/Y")}}</span>
                                    <h5 class="title"><a href="/blog/{{ $item->slug }}">{{ $item->title}}</a></h5>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Categories</h4>
                        <div class="category-widget-box">
                            <ul class="category-list">
                                @foreach ($categories as $item)
                                <li><a href="/blog?category={{$item->name}}"><i class="fas fa-chevron-right"></i> {{ $item->name }}
                                    <span>{{$item->published_blogs->count()}}</span></a></li>    
                                @endforeach
                        </div>
                    </div>
                    <div class="widget-box">
                        <h4 class="widget-title">Tags</h4>
                        <div class="tag-widget-box">
                            <ul class="tag-list">
                                @foreach ($tags as $item)
                                    <li><a href="/blog?tag={{ $item }}">{{$item}}</a></li>
                                    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Blog
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@push("scripts")
<script>
    function reply(comment_id) {
            $("#parent_id").val(comment_id)
        }
    $(".alert-wrapper").click(function() {
        $(this).remove()
    })


</script>
@endpush
<!--blog single end-->
@endsection
