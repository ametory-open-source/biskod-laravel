@extends("layouts.contact")

@section('content')

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="banner-section two inner">
    <div class="banner-element-four two">
        <img src="/web/assets/images/element/element-5.png" alt="element">
    </div>
    <div class="banner-element-five two">
        <img src="/web/assets/images/element/element-7.png" alt="element">
    </div>
    <div class="banner-element-nineteen two">
        <img src="/web/assets/images/element/element-6.png" alt="element">
    </div>
    <div class="banner-element-twenty-two two">
        <img src="/web/assets/images/element/element-69.png" alt="element">
    </div>
    <div class="banner-element-twenty-three two">
        <img src="/web/assets/images/element/element-70.png" alt="element">
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center mb-30-none">
            <div class="col-xl-12 mb-30">
                <div class="banner-content two">
                    <div class="banner-content-header">
                        <h2 class="title">Blog</h2>
                        <div class="breadcrumb-area">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="/blog">Blog</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<a href="#" class="scrollToTop"><i class="las la-angle-double-up"></i></a>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Blog
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="blog-section ptb-120">
    <div class="container">
        <div class="row justify-content-center mb-60-none">
            <div class="col-xl-8 col-lg-8 mb-60">
                <div class="row justify-content-center mb-60-none">
                    @foreach ($blogs as $item)
                    <div class="col-xl-12 mb-60">
                        <div class="blog-item">
                            <div class="blog-thumb"   style="background: url({{assetUrl($item->feature_image)}}) no-repeat; height: 360px;background-size: cover;">
                                
                            </div>
                            <div class="blog-content">
                                <div class="blog-post-meta">
                                    <span class="user">By : {{$item->author}}</span>
                                    <span class="category two">{{$item->created_at->format("d/M/Y")}}</span>
                                </div>
                                <h3 class="title"><a href="/blog/{{$item->slug}}">{{$item->title}}</a></h3>
                                <p>{{$item->short_description}}</p>
                                <div class="blog-btn">
                                    <a href="/blog/{{$item->slug}}" class="custom-btn">Read More <i
                                            class="fas fa-arrow-right ml-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
            <div class="col-xl-4 col-lg-4 mb-60">
                <div class="sidebar">
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Search</h4>
                        <div class="search-widget-box">
                            <form class="search-form" action="/blog">
                                <input type="text" name="search" class="form--control" placeholder="Search" >
                                <button type="submit"><i class="icon-Search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Recent Posts</h4>
                        <div class="popular-widget-box">
                            @foreach ($recents as $item)
                            <div class="single-popular-item d-flex flex-wrap align-items-center">
                                <div class="popular-item-thumb">
                                    <img src="{{ assetUrl($item->feature_image) }}"
                                        alt="{{ $item->feature_image_caption }}">
                                </div>
                                <div class="popular-item-content">
                                    <span class="blog-date">{{ $item->created_at->format("d/M/Y")}}</span>
                                    <h5 class="title"><a href="/blog/{{ $item->slug }}">{{ $item->title}}</a></h5>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                    <div class="widget-box mb-30">
                        <h4 class="widget-title">Categories</h4>
                        <div class="category-widget-box">
                            <ul class="category-list">
                                @foreach ($categories as $item)
                                <li><a href="/blog?category={{$item->name}}"><i class="fas fa-chevron-right"></i> {{ $item->name }}
                                    <span>{{$item->published_blogs->count()}}</span></a></li>    
                                @endforeach
                        </div>
                    </div>
                    <div class="widget-box">
                        <h4 class="widget-title">Tags</h4>
                        <div class="tag-widget-box">
                            <ul class="tag-list">
                                @foreach ($tags as $item)
                                    <li><a href="/blog?tag={{ $item }}">{{$item}}</a></li>
                                    
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ $blogs->links('static.paginator') }}
      

    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Blog
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--blog single end-->
@endsection
@push("scripts")
<script>
    function(ev) {

    }
</script>
@endpush