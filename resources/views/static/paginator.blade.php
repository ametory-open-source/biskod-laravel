@if ($paginator->hasPages())
<nav>
    <ul class="pagination">
        @if ($paginator->onFirstPage())
        <li class="page-item prev">
            <a class="page-link" href="#" rel="prev" aria-label="Prev &raquo;">PREV</a>
        </li>
        @else
        <li class="page-item prev">
            <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                aria-label="Prev &raquo;">PREV</a>
        </li>
        @endif
        @foreach ($elements as $element)
        {{-- "Three Dots" Separator --}}
            @if (is_string($element))
            <li class="page-item disabled" aria-disabled="true"><span class="page-item">{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active" aria-current="page"><span class="page-link">{{ str_pad($page, 2, '0', STR_PAD_LEFT) }}</span></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ str_pad($page, 2, '0', STR_PAD_LEFT) }}</a></li>
                @endif

                @endforeach
            @endif
        
        @endforeach


        

        @if ($paginator->hasMorePages())
        <li class="page-item next">
            <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="Next &raquo;">NEXT</a>
        </li>
        @else
        <li class="page-item next">
            <a class="page-link" href="#" rel="next" aria-label="Next &raquo;">NEXT</a>
        </li>
        @endif
    </ul>
</nav>
@endif