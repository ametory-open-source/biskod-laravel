@extends("layouts.contact")

@section('content')

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="banner-section two inner">
    <div class="banner-element-four two">
        <img src="/web/assets/images/element/element-5.png" alt="element">
    </div>
    <div class="banner-element-five two">
        <img src="/web/assets/images/element/element-7.png" alt="element">
    </div>
    <div class="banner-element-nineteen two">
        <img src="/web/assets/images/element/element-6.png" alt="element">
    </div>
    <div class="banner-element-twenty-two two">
        <img src="/web/assets/images/element/element-69.png" alt="element">
    </div>
    <div class="banner-element-twenty-three two">
        <img src="/web/assets/images/element/element-70.png" alt="element">
    </div>
    <div class="container">
        <div class="row justify-content-center align-items-center mb-30-none">
            <div class="col-xl-12 mb-30">
                <div class="banner-content two">
                   <div class="banner-content-header">
                        <h2 class="title">Contact us</h2>
                        <div class="breadcrumb-area">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact us</li>
                                </ol>
                            </nav>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Banner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<a href="#" class="scrollToTop"><i class="las la-angle-double-up"></i></a>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Scroll-To-Top
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Contact-item
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<section class="contact-item-section ptb-120">
    <div class="contact-item-element-one">
        <img src="/web/assets/images/element/element-71.png" alt="element">
    </div>
    <div class="contact-item-element-two">
        <img src="/web/assets/images/element/element-72.png" alt="element">
    </div>
    <div class="container">
        <div class="row justify-content-center mb-30-none">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 mb-30">
                <div class="contact-item text-center">
                    <div class="contact-icon-area">
                        <div class="contact-icon">
                            <i class="las la-map-marked-alt"></i>
                        </div>
                    </div>
                    <div class="contact-content">
                        <h3 class="title">Visit Our Office</h3>
                        <p>{{ $web->company_address }}</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 mb-30">
                <div class="contact-item text-center">
                    <div class="contact-icon-area">
                        <div class="contact-icon">
                            <i class="las la-phone"></i>
                        </div>
                    </div>
                    <div class="contact-content">
                        <h3 class="title">Call Us</h3>
                        <p><a href="tel:{{ $web->company_phone }}">{{ $web->company_phone }}</a> </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 mb-30">
                <div class="contact-item text-center">
                    <div class="contact-icon-area">
                        <div class="contact-icon">
                            <i class="las la-envelope-open-text"></i>
                        </div>
                    </div>
                    <div class="contact-content">
                        <h3 class="title">Mail Us</h3>
                        <p><a href="mailto:{{ $web->company_email }}">{{ $web->company_email }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Contact-item
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Contact
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div class="contact-section ptb-120">
    <div class="contact-element-one">
        <img src="/web/assets/images/element/element-73.png" alt="element">
    </div>
    <div class="contact-element-two">
        <img src="/web/assets/images/element/element-74.png" alt="element">
    </div>
    <div class="container">
        <form class="contact-form">
            <div class="row justify-content-center mb-25-none">
                <div class="col-xl-6 col-lg-6 form-group">
                    <label>Enter Name</label>
                    <input type="text" name="name" class="form--control" placeholder="Fullname" value="{{ old('name') }}">
                </div>
                <div class="col-xl-6 col-lg-6 form-group">
                    <label>Email Address</label>
                    <input type="email" name="email" class="form--control" placeholder="jhonsmith@gmail.com" value="{{ old('email') }}">
                </div>
                <div class="col-xl-6 col-lg-6 form-group">
                    <label>Your Phone</label>
                    <input type="text" name="number" class="form--control" placeholder="ex: +62-813-696-3600" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /
                    value="{{ old('phone') }}">
                </div>
                <div class="col-xl-6 col-lg-6 form-group">
                    <label>Select Subject</label>
                    <div class="contact-select">
                        <select class="form--control" name="title" placeholder="Select title">
                            @foreach (config("landing.contact_titles") as $item)
                                <option value="{{$item}}" {{ old('title') == $item ? 'SELECTED' : null }}>{{$item}}</option>    
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 form-group">
                    <label>Write Message</label>
                    <textarea name="message" class="form--control" placeholder="Write Here ...">{{ old('description') }}</textarea>
                </div>
                <div class="col-xl-12 col-lg-12 form-group text-center">
                    <button type="submit" class="btn--base mt-20">Send Now <i class="fas fa-arrow-right ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Contact
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div class="map-area">
    {!! $web->company_map !!}
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

@endsection
@push("scripts")
<script>
    @if ($message = Session::get('success'))
        alert("Thank you for your participation")
    @endif
    @if ($message = Session::get('error'))
        alert("Something error, we cannot receive your message")
    @endif
   
</script>
@endpush
@push('css')
    <style>
        .map_wrapper {
            width: calc(100% - 10rem);
            margin: 0 auto;
        }
        .map_wrapper iframe {
            border-radius: 3rem;
        }
        address .fas {
            color: #999;
        }
    </style>
@endpush